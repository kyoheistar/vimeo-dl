require 'rubygems'
require "nokogiri"
require "open-uri"
require 'json'
require 'uri'
require 'net/http'

def getMainJson( needle )
	@targets
	begin
		youtubeDocument = Nokogiri::HTML(open( needle['link'] ),nil,"utf-8")
		case needle['site']
			when 'vimeo'
				mainScript = youtubeDocument.xpath( needle['xpath'] ).attribute( needle['keyword'] )
				vimeoDocument = Nokogiri::HTML(open(mainScript),nil,"utf-8")
				@targets = vimeoDocument.xpath('//p').text
		end

	rescue => e
		#問題があればここでメールを飛ばすよいかも
		puts e.message
	end
	if @targets.nil?
		#問題があればここでメールを飛ばすよいかも
		p '見つかりませんでした ERROR002'
		return nil
	else
		return @targets
	end
end

def youtubeAnalysis( json , needle )
	reUrl = []
	#mp4の箇所を探し
	case needle['site']
	when 'vimeo'
		reUrl.push( json[ needle['targetKey'][0]  ][ needle['targetKey'][1] ][ needle['targetKey'][2] ][ needle['targetKey'][3] ][ needle['targetKey'][4] ] )
	end
	# devidUrl.each do | line |
	# 	#不要箇所を削除
	# 	# p line
	# 	if URI.unescape(line).match('video/mp4')
	# 		reUrl.push( URI.unescape(line) )
	# 	end
	# end

	if reUrl.length
		return reUrl[0]
	else
		p '見つかりませんでした ERROR003'
		return reUrl
	end
end

seach = {
	'site' => 'vimeo' ,
	'link' => 'http://vimeo.com/channels/staffpicks/98437579',
	'keyword' => 'data-config-url',
	'xpath' => '//div[@class="player"]',
	'targetKey' => [ 'request','files','h264', 'hd' ,'url']
}

begin
	#youtubeのhtmlからjavascriptの必要箇所のみ取り出す
	youtubeText = getMainJson( seach )
	#youtubeのjavascriptから取得したJSONを取り扱う
	youtubeJSON = JSON.parse( youtubeText )
	#youtubeのjson解析後 mp4の物だけ抽出
	resultUrl = youtubeAnalysis( youtubeJSON , seach )

	case seach['site']
		when 'vimeo'
			pureUrl = resultUrl
	end
rescue Exception => e
	#問題があればここでメールを飛ばすよいかも
	puts e.message
end

test = '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body><video controls autoplay >' + '<source src=\'' + pureUrl + '\'>' + '</video></body></html>'

File.write("test_vimeo.html", test)